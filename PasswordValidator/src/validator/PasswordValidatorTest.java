package validator;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PasswordValidatorTest {

	@Test
	void testCheckPasswordLength() {
		boolean result = PasswordValidator.checkPasswordLength("Passwords");
		assertTrue("Password Validated", result == true);
	}
	
	@Test
	void testCheckPasswordLengthException() 
	{
		boolean result = PasswordValidator.checkPasswordLength("hello");
		assertTrue("Password not validated", result == false);
		
	}
	
	@Test
	void testCheckPasswordLengthBoundryIn() 
	{
		boolean result = PasswordValidator.checkPasswordLength("Password");
		assertTrue("Password not validated", result == true);
		
	}
	
	@Test
	void testCheckPasswordLengthBoundryOut() 
	{
		boolean result = PasswordValidator.checkPasswordLength("Passwor");
		assertTrue("Password not validated", result == false);
		
	}

	@Test
	void testCheckDigits() {
		boolean result = PasswordValidator.checkDigits("Password123");
		assertTrue("Password validated", result == true);
	}
	
	@Test
	void testCheckDigitsException() {
		boolean result = PasswordValidator.checkDigits("Password");
		assertTrue("Password not validated", result == false);
	}
	
	@Test
	void testCheckDigitsBoundryIn() {
		boolean result = PasswordValidator.checkDigits("Password12");
		assertTrue("Password validated", result == true);
	}
	
	@Test
	void testCheckDigitsBoundryOut() {
		boolean result = PasswordValidator.checkDigits("Password1");
		assertTrue("Password not validated", result == false);
	}

}

package validator;

public class PasswordValidator {
	
//method to check the password length is 8 character
	public static boolean checkPasswordLength(String password) 
	{
		if(password.length() >=8)
		return true;
		
		else
		return false;
		
	}
	
	
	
//method to check the password has atleast two digits
	public static boolean checkDigits(String password)
	{
		int digitCount = 0;
		Boolean isDigit = false;
		
	//check if char is digit
		for(int i = 0; i< password.length(); i++){
			{
				isDigit = Character.isDigit(password.charAt(i));
				if(isDigit) digitCount++; // increment the counter, if it is a digit
			}
		}
		
		if(digitCount >=0)
		{
			return true;
		}
		
		else
			return false;
	}
	


}